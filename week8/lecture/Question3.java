package lecture;

public class Question3 {
	String str = "Merhaba";

	public static void main(String[] args) {
		Question3 b = new Question3();
		b.str = "Hello";
		Question3 c = b;
		c.str = new String("Hola");
		
		System.out.println("b.str" + b.str);
		System.out.println("c.str" + c.str);
		
		Question3 d = new Question3();
		d.str = b.str;
		
		b.str = "Thank You";
		
		System.out.println("d.str" + d.str);
		System.out.println("c.str" + c.str);
	}
}