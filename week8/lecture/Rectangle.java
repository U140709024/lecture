package lecture;

public class Rectangle {

	protected int length=5, width=5;


	
	public Rectangle(int length, int width) {
		super();
		System.out.println("Rectangle is created");
		this.length = length;
		this.width = width;
	}

	
	public String toString() {
		return "length = " + length + " width = "+width;
	}
	
	protected  int area() {
		return length * width;
	}
	
	
	
	public void setLength(int value) {
		length = value;
	}
	
	public int getLength() {
		return length;
	}


	public int getWidth() {
		return width;
	}


	public void setWidth(int width) {
		this.width = width;
	}


	
	
	
}
