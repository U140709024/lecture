package lecture;

public class Box  extends Rectangle{

	int  height;
	

	
	 public Box() {
		super(5,4);
		
	}

	public int area() {
		
		return 2 * (super.length * getWidth() + getLength() * height + getWidth() * height);
	}
	
	public int volume() {
		return super.area() * height;
	}
	

	public int getHeight() {
		return height;
	}


	public void setHeight(int height) {
		this.height = height;
	}
	
	
	
	
}
