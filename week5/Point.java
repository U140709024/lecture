

public class Point {
	
	int xCoord = 1;
	int yCoord = 1;

	public Point() {
		System.out.println("Creating Point");
	}
	
	
	public Point(int x, int y) {
		xCoord = x;
		yCoord = y;
		System.out.println("Creating Point " + x + ", "+y);
	}
	
	public void move(int xDistance, int yDistance) {
		xCoord += xDistance;
		yCoord += yDistance;
	}
	
	public double distanceFromOrigin() {
		return Math.sqrt(xCoord*xCoord + yCoord*yCoord);	
	}
	
	public static double distFromOrigin(Point p) {
		return  Math.sqrt(p.xCoord*p.xCoord + p.yCoord*p.yCoord);
	}
	
	
	public static void main(String[] args) {
		
		Point points[] = new Point[8];
		points[0] = new Point(2,3);
		points[1] = new Point();
		points[2] = new Point();
		points[3] = new Point();
		points[4] = new Point(3,4);
		points[5] = new Point();
		points[6] = new Point();
		points[7] = new Point();
		
		System.out.println("Coord " + points[0].xCoord + ", "+points[0].yCoord);
		
		points[0].move(5, -5);
		
		System.out.println("Coord " + points[0].xCoord + ", "+points[0].yCoord);
		
		for(int i = 0; i < 8; i++) {
			System.out.println(points[i].distanceFromOrigin());
		}
		
		
		
		
		Point p = new Point(5,6);
		p.distanceFromOrigin();
		
		distFromOrigin(p);
		
	}

}
