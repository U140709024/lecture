
public class NestedLoop {

	public static void main(String[] args) {
		 outer: for (int i = 0; i < 3; i++) {
			 	
				for (int j = 1; j < 5; j++) {
					if (j == 3) {
						break outer;
					}
					System.out.println(i + "," + j);
			
			}
		}

	}

}
