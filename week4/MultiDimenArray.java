
public class MultiDimenArray {

	public static void main(String[] args) {
		int[][] matrix = {{5,3,5},{7,3,5},{5,3,8}};
		
		int[] row = matrix[1];
		
		System.out.println(matrix[1]);
		
		for (int i = 0; i < matrix[1].length; i++) {
			//System.out.print(matrix[1][i] + " ");
		}
		
		int sum = 0;
		for (int rw = 0; rw < 3; rw++) {
			for (int col = 0; col < 3; col++) {
				sum += matrix[rw][col];
			}
		}

		System.out.println(sum);
		
	}

}
