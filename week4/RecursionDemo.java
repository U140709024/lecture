
public class RecursionDemo {

	public static void main(String[] args) {
		
		int r = factorialLoop(0);
		
		System.out.println(r);
		
		r = factorialRec(6);
		System.out.println(r);
		
		System.out.println(fibonacciLoop(5));
		System.out.println(fibonacciLoop(6));
		System.out.println(fibonacciLoop(7));
		
		System.out.println(fibonacciRec(5));
		System.out.println(fibonacciRec(6));
		System.out.println(fibonacciRec(7));

	}
	
	private static int factorialRec(int number) {
		if (number == 0)
			return 1;
		return number * factorialRec(number -1);
	
	}
	

	private static int factorialLoop(int number) {
		
		int result = 1;
		
		for (int i =1; i <= number; i++) {
			result*= i;
		}
		return result;
		
	}

	private static int fibonacciRec(int number) {
		if (number ==0 || number ==1) {
			return 1;
		}
		
		return fibonacciRec(number - 1) + fibonacciRec(number - 2); 
	}
	
	
	
	private static int fibonacciLoop(int number) {
		int result = 1;
		
		int fib1 = 1;
		int fib2 = 1;
		
		for (int i = 1; i <  number; i++) {
			result = fib1 + fib2;
			fib1 = fib2;
			fib2 = result;
		}
				
		return result;
		
		
	}
	
}
