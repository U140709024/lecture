package lecture;

public class ParameterPassing2 {

	int z;
	
	public static void main(String[] args) {
		int x = 5;
		int y = 7;
		ParameterPassing2 p2 = new ParameterPassing2();
		p2.z = 9;
		
		
		ParameterPassing2 p3 = p2;
		
		p3.z = 12;
		
		System.out.println("main: p2.z = " + p2.z );
		
		
		
		p2.foo(x,y);
		
		System.out.println("main : x = " + x + " y = " +y);
		
		ParameterPassing2 p = new ParameterPassing2();
		p.z = 8;
		
		foo2(p);
		

		System.out.println("foo2: p.z = " + p.z );
	}
	
	public static void foo2(ParameterPassing2 p) {
		p.z++;
		
		System.out.println("foo2: p.z = " + p.z );
	}
	
	public  void foo(int x, int y) {
		x+=10;
		y--;
		
		System.out.println("foo: x = " + x + " y = " +y);
	}

	
	
	
}
