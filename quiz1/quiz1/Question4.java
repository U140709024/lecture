package quiz1;


public class Question4 {

	public static void main(String[] args) {
		
		int a = 5;
		int b = a++;
		boolean bool = (b==5) || (7 == ++a);
		
		int c = bool ? --b : a++;
		
		int d = a++ + --b + c++;
		
		System.out.println("a="+a+" b="+b+" c="+c+" d="+d);

				
		
	}

}
