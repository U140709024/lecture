

public class Precedence {

	public static void main(String[] args) {
		int i = 2;
		
		boolean bool = (i < 5) || (i < 3) && (++i < 3);
		
		//boolean bool = (++i < 3) &&(i < 5) || (i < 3) ;
		
		System.out.println("bool = " + bool + " i = " + i);
		
		
		int x = number2() + number3() * 5;

	}
	
	
	public static int number3() {
		System.out.println("number3 is called");
		return 3;
	}

	
	public static int number2() {
		System.out.println("number2 is called");
		return 2;
	}
}
