package quiz3;

public class Time {
	private int second;
	private int minute;
	private int hour;

	public Time(int second, int minute, int hour) {
		super();
		this.second = second;
		this.minute = minute;
		this.hour = hour;
	}


	public void incrementSecond() {
		++second;
		if (second == 60) {
			second = 0;
			incrementMinute();
		}
	}
	public void incrementMinute() {
		++minute;
		if (minute == 60) {
			minute = 0;
			incrementHour();
		}
	}	
	
	public void incrementHour() {
		++hour;
		if (hour == 24) {
			hour = 0;
		}
	}
}