

public class TestPoint {
	
	public static void main(String[] args) {
		Point2D p = new Point2D(5,7);
		
		System.out.println(p.toString());
		
		
		Point3D p3 = new Point3D(5,8,9);
		
		p3.move(3, 3, 3);
		System.out.println(p3.toString());
		
		
		Point2D p2 = p3;
		
		p2.move(3, 3);
		
		Object o = p3;
		
		//o = new String("Merhaba");
		
		o.toString();
		
		if (o instanceof Point3D) {
			Point3D p3d = (Point3D)o;
			System.out.println("Type of o object is" + o.getClass().getName());
		}else {
			System.out.println("Type of o object is " + o.getClass().getName());
		}
		
		
		
		Object o2 = new String("Hello");
		
		//Point3D p123 = (Point3D)o2;
		
	}

}
